package JavaADF2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Contact {
    Scanner sc = new Scanner(System.in);
    HashMap<String,Contact> hashMap = new HashMap<>();
    ArrayList<String> namekey = new ArrayList<>();
    ArrayList<String> phonekey = new ArrayList<>();
    private String name;
    private String phone;

    public Contact() {

    }



    public Contact(String name, String phone) {
        this.name = name;
        this.phone = phone;
    }

    public Scanner getSc() {
        return sc;
    }

    public void setSc(Scanner sc) {
        this.sc = sc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void insertContact(){

        Contact contact = new Contact();
        System.out.println("----Nhập thông tin liên hệ----");
        System.out.println("Tên:");
        this.name = sc.nextLine();
          contact.setName(this.name);
        System.out.println("Số điện thoại");
        this.phone = sc.nextLine();
        contact.setPhone(this.phone);
       hashMap.put(phone,contact);
    }
    public void searchByName(){
        System.out.println("Nhập tên bạn muốn tìm:");
        String namefind = sc.nextLine();
        for (int i = 0; i < hashMap.size() ; i++) {
            for (Contact contact:hashMap.values()
            ) {
namekey.add(contact.getName());
phonekey.add(contact.getPhone());
            }
            if (namefind.equalsIgnoreCase(namekey.get(i))){
                System.out.println("Tên: "+hashMap.get(phonekey.get(i)).getName());
                System.out.println("Số Điện Thoại: "+hashMap.get(phonekey.get(i)).getPhone());
            }else {
                System.err.println("Không tồn tại người này!");
            }
        }
        sc.nextLine();
    }
    public void findAll(){
        System.out.println("THÔNG TIN BẠN CẦN TÌM LÀ :\n");
        for (int i = 0; i < hashMap.size(); i++) {
            for (Contact contact:hashMap.values()
            ) {
                namekey.add(contact.getName());
                phonekey.add(contact.getPhone());
            }
            System.out.println("Tên: "+hashMap.get(phonekey.get(i)).getName());
            System.out.println("Số Điện Thoại: "+hashMap.get(phonekey.get(i)).getPhone());
        }
        sc.nextLine();
    }
   public void getMenu() {
       while (true){
           System.out.println("-----------===Menu===-----------");
           System.out.println("1.Thêm liên hệ");
           System.out.println("2.Tìm liên hệ theo tên");
           System.out.println("3.Hiển thị tất cả liên hệ có sẵn");
           System.out.println("4.Thoát");
           System.out.println("Mời bạn chọn: ");
           String choice = sc.nextLine();
           switch (choice){
               case"1":insertContact();
                   break;
               case"2":searchByName();
                   break;
               case"3":findAll();
                   break;
               case"4":
                   System.out.println("Hẹn gặp bạn lần sau!");
                   break;
               default:
                   System.err.println("bạn không chọn yêu cầu nào cả");
                   break;
           }
           if (choice.equals("4")){
               break;
           }
       }

       }
}
